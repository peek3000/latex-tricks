#include <iostream>
#include <cstdio>
#include <map>
#include <vector>
#include <string>

using namespace std;

// To be thorough, all numbers between 2 and 2*3*5*7*11*13*17*19 must be
// tested.
//
int f(int x) {
	int n = 1;
	if (!( x % 2 )) n *= 2;
	if (!( x % 3 )) n *= 3;
	if (!( x % 5 )) n *= 5;
	if (!( x % 7 )) n *= 7;
	if (!( x % 11 )) n *= 11;
	if (!( x % 13 )) n *= 13;
	if (!( x % 17 )) n *= 17;
	if (!( x % 19 )) n *= 19;
	return(n);
}

int main(int argc, char ** argv)
{
	int x = 0;
	int x_max = 0;
	int n = 0;
	map<int,char> m;
	map<int,char>::iterator mi;

	x_max = 1;
	x_max *= 2;
	x_max *= 3;
	x_max *= 5;
	x_max *= 7;
	x_max *= 11;
	x_max *= 13;
	x_max *= 17;
	x_max *= 19;

	for (x = 2; x <= x_max; x++) {
		n = f(x);
		if (m.find(n) == m.end()) m.insert(make_pair(n,'a'));
	}

	cout << m.size() << endl;
	for (mi = m.begin(); mi != m.end(); mi++)
		cout << mi->first << " ";
	cout << endl;
}
