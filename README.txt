This is a list of some LaTeX tricks I've picked up over the last few years,
as well as a few examples that I've seen others do as well.

Example.pdf -- All of the tricks and examples
Example.tex -- The source file that produces the PDF
code/, data/ -- Extra directories with stuff included by Example.tex
Makefile -- Got this online
Makefile.ini -- My additional Make directives, this file is optional
watchme -- A script that watches Example.tex for changes, and then runs make

Bare Minimum:
- Download Example.tex, code/, data/, Makefile
- Type: make
